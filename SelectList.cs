using System;
using System.Collections.Generic;
namespace vsco
{
    class SelectList
    {
        private int currentCursol;
        private int cursorTop;
        private int cursorBottom;
        private int indentLevel;
        private List<string> viewList;
        private ConsoleColor inactiveForegroundColor;
        private ConsoleColor inactiveBackgroundColor;
        public ConsoleColor activeForegroundColor;
        public ConsoleColor activeBackgroundColor;

        public SelectList(List<string> source, int indent = 0)
        {
            currentCursol = 0;
            indentLevel = indent;
            viewList = source;
            activeForegroundColor = ConsoleColor.Black;
            activeBackgroundColor = ConsoleColor.White;
            inactiveForegroundColor = Console.ForegroundColor;
            inactiveBackgroundColor = Console.BackgroundColor;
        }

        public int showList()
        {
            var flg = true;
            viewList.ForEach( x => {
                Console.CursorLeft = indentLevel;
                Console.WriteLine(x);
            });
            cursorBottom = Console.CursorTop - 1;
            cursorTop = Console.CursorTop - viewList.Count;
            Console.CancelKeyPress += new ConsoleCancelEventHandler(CancelEvent);
            while (flg)
            {
                Console.CursorTop = cursorTop + currentCursol;

                Console.BackgroundColor = activeBackgroundColor;
                Console.ForegroundColor = activeForegroundColor;
                Console.CursorLeft = indentLevel;
                Console.Write(viewList[currentCursol]);
                Console.CursorLeft = indentLevel;

                var key = Console.ReadKey();
                switch (key.Key)
                {
                    case ConsoleKey.Enter:
                        Console.BackgroundColor = inactiveBackgroundColor;
                        Console.ForegroundColor = inactiveForegroundColor;
                        flg = false;
                        break;
                    case ConsoleKey.UpArrow:
                        Console.BackgroundColor = inactiveBackgroundColor;
                        Console.ForegroundColor = inactiveForegroundColor;
                        Console.CursorLeft = indentLevel;
                        Console.Write(viewList[currentCursol]);
                        Console.CursorLeft = indentLevel;
                        if (Console.CursorTop != cursorTop)
                        {
                            currentCursol--;
                        }
                        break;
                    case ConsoleKey.DownArrow:
                        Console.BackgroundColor = inactiveBackgroundColor;
                        Console.ForegroundColor = inactiveForegroundColor;
                        Console.CursorLeft = indentLevel;
                        Console.Write(viewList[currentCursol]);
                        Console.CursorLeft = indentLevel;
                        if (Console.CursorTop != cursorBottom)
                        {
                            currentCursol++;
                        }
                        break;
                    default: break;
                }
            }
            Console.CursorTop = cursorBottom + 1;
            return currentCursol;
        }

        private void CancelEvent(object sender, ConsoleCancelEventArgs args)
        {
            Console.BackgroundColor = inactiveBackgroundColor;
            Console.ForegroundColor = inactiveForegroundColor;
            Console.CursorTop = cursorBottom + 1;
            Console.WriteLine();
        }
    }
}