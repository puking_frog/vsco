﻿using System;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Collections.Generic;
using System.Diagnostics;
using Clitool;
namespace vsco
{
    class Program
    {
        static FileInfo settingFile = ReadSetting();
        static SortedDictionary<string, string> setting;
        static JsonSerializerOptions jOption = new JsonSerializerOptions()
        {
            MaxDepth = 10,
            WriteIndented = true
        };
        static void Main(string[] args)
        {
            setSetting();
            var cli = new Cli();
            cli.name = "vsco";


            #region select Command

            // エイリアス一覧からvscodeを開く
            var selectCommand = new Command();
            selectCommand.name = null;
            selectCommand.alias = new List<string>() { "select", "-s" };
            selectCommand.description = "一覧からエイリアス選択を選択し、VsCodeで開く";
            selectCommand.execute = (List<string> option) =>
            {
                Select();
            };

            #endregion


            #region open Command

            // パスを直接指定して開く
            var opneCommand = new Command();
            opneCommand.name = null;
            opneCommand.description = "VsCodeで指定のパスを開く";
            opneCommand.alias = new List<string>(){
                "open",
                "-o"
            };
            opneCommand.options.Add(new CommandOption()
            {
                name = "path",
                description = "VsCodeで開くパス",
                require = true
            });
            opneCommand.execute = (List<string> options) =>
            {
                string path = options[0];
                Open(path);
            };

            #endregion


            #region add Command

            // エイリアスの登録
            var AddCommand = new Command();
            AddCommand.name = null;
            AddCommand.description = "エイリアスを登録する";
            AddCommand.alias = new List<string>(){
                "add",
                "-a"
            };
            AddCommand.options.Add(new CommandOption()
            {
                name = "alias",
                description = "登録するエイリアス",
                require = true
            });
            AddCommand.options.Add(new CommandOption()
            {
                name = "path",
                description = "登録するパス",
                require = true
            });
            AddCommand.execute = (List<string> options) =>
            {
                string alias = options[0];
                string path = options[1];
                bool isUsed = cli.commands.Where(x => x.name?.Equals(alias) ?? false || x.alias.Contains(alias)).Any();
                if (isUsed)
                {
                    throw new Exception("指定のエイリアスはvscoコマンドで既に使用されているため、登録できません");
                }
                Add(alias, path);
            };

            #endregion


            #region remove Command

            // エイリアスの削除
            var removeCommand = new Command();
            removeCommand.name = "remove";
            removeCommand.description = "エイリアスの削除";
            removeCommand.alias = new List<string>(){
                "-r"
            };
            removeCommand.options.Add(new CommandOption()
            {
                name = "alias",
                description = "削除するエイリアス",
                require = true
            });
            removeCommand.execute = (List<string> options) =>
            {
                var alias = options[0];
                Remove(alias);
            };


            #endregion


            #region list Command

            // リストの表示
            var listCommand = new Command();
            listCommand.name = "list";
            listCommand.description = "登録済みのエイリアスの一覧表示";
            listCommand.alias = new List<string>(){
                "-l"
            };
            listCommand.execute = (List<string> options) =>
            {
                ShowList();
            };


            #endregion


            #region vscoSetting Command

            // 設定ファイルを開く
            var settingCommand = new Command();
            settingCommand.name = "vscoSetting";
            settingCommand.alias = new List<string>(){
                "-vs"
            };
            settingCommand.description = "vsco設定ファイルを開く（登録済みエイリアスの一覧のjsonファイル）";
            settingCommand.execute = (List<string> options) =>
            {
                OpenSetting();
            };

            #endregion


            #region usableAlias Command

            var reservedCommand = new Command();
            reservedCommand.name = "usableAlias";
            reservedCommand.alias = new List<string>(){
                "-ua"
            };
            reservedCommand.description = "使用できないエイリアスの一覧表示";
            reservedCommand.execute = (List<string> options) =>
            {

                Console.WriteLine("\n 以下の文字列はエイリアスとして使用できません\n");

                var list = cli.commands.Where(x => !x.name?.Equals(null) ?? false).Select(x => x.name).ToList();
                cli.commands.ForEach(x => list.AddRange(x.alias));

                list.Sort();
                list.Select(x => $"\t{x}").ToList().ForEach(Console.WriteLine);
                Console.WriteLine();
            };

            #endregion


            #region rename Command

            // alias変更コマンド
            var renameCommand = new Command();
            renameCommand.name = "rename";
            renameCommand.alias = new List<string>(){
                "-rn"
            };
            renameCommand.description = "エイリアスの変更";
            renameCommand.options.Add(new CommandOption()
            {
                name = "currentAlias",
                description = "現在のエイリアス",
                require = true
            });
            renameCommand.options.Add(new CommandOption()
            {
                name = "newAlias",
                description = "新しいエイリアス",
                require = true
            });
            renameCommand.execute = (List<string> options) =>
            {

                var oldAlias = options[0];
                var newAlias = options[1];
                var path = setting.GetValueOrDefault(oldAlias);

                bool isUsed = cli.commands.Where(x => x.name?.Equals(newAlias) ?? false || x.alias.Contains(newAlias)).Any();

                if (isUsed)
                {
                    Console.WriteLine("指定のエイリアスはvscoコマンドで既に使用されているため、登録できません");
                    return;
                }
                try
                {
                    Remove(oldAlias);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return;
                }
                setting.Add(newAlias, path);
                File.WriteAllText(settingFile.FullName, JsonSerializer.Serialize(setting, jOption));
            };

            #endregion


            cli.commands.AddRange(
                new List<Command>(){
                    selectCommand,
                    opneCommand,
                    AddCommand,
                    removeCommand,
                    listCommand,
                    settingCommand,
                    reservedCommand,
                    renameCommand
                }
            );
            cli.executeCommand(args);
        }

        static void Add(string alias, string path)
        {

            var value = setting.GetValueOrDefault(alias);
            if (value != null)
            {
                setting.Remove(alias);
            }

            var fullPath = Path.GetFullPath(path);

            setting.Add(alias, fullPath);
            File.WriteAllText(settingFile.FullName, JsonSerializer.Serialize(setting, jOption));
        }

        static void Remove(string alias)
        {
            if (!settingFile.Exists)
            {
                throw new Exception("設定ファイルが存在しません。");
            }
            var value = setting.GetValueOrDefault(alias);
            if (value == null)
            {
                throw new Exception("指定のエイリアスは登録されていません。");
            }
            setting.Remove(alias);
            File.WriteAllText(settingFile.FullName, JsonSerializer.Serialize(setting, jOption));
        }

        static void ShowList()
        {
            if (!settingFile.Exists)
            {
                init();
            }

            if (setting.Keys.Count > 0)
            {
                Console.WriteLine();
                foreach (var item in setting)
                {
                    Console.WriteLine($"\t{item.Key}: {item.Value}");
                }
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("登録されたエイリアスはありません。");
            }
        }

        static void OpenSetting()
        {
            if (!settingFile.Exists)
            {
                init();
            }
            OpenCode(settingFile.FullName);
        }

        static void Open(string option)
        {
            var target = setting.GetValueOrDefault(option) ?? option;
            var directory = new DirectoryInfo(target);
            var file = new FileInfo(target);

            if (directory.Exists || file.Exists)
            {
                OpenCode(directory.FullName);
            }
            else
            {
                Console.WriteLine("無効なエイリアス、または指定のファイル・ディレクトリが見つかりません。");
            }
        }

        static void OpenCode(string target)
        {
            Process process = new Process();
            process.StartInfo.FileName = @"powershell.exe";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.Arguments = $"code {target}";
            process.Start();
            process.Close();
        }

        static void init()
        {
            File.WriteAllText(settingFile.FullName, JsonSerializer.Serialize(setting, jOption));
        }

        static void setSetting()
        {
            if (settingFile.Exists)
            {
                setting = JsonSerializer.Deserialize<SortedDictionary<string, string>>(File.ReadAllText(settingFile.FullName));
            }
            else
            {
                setting = new SortedDictionary<string, string>();
            }
        }
        static void Select()
        {

            var selectList = new List<string>();
            foreach (var s in setting)
            {
                selectList.Add($"{s.Key}: {s.Value}");
            }

            if (setting.Count > 0)
            {
                var list = new SelectList(selectList, 2);
                Console.WriteLine();
                var select = list.showList();
                Console.WriteLine();
                Open(setting.ToList()[select].Value);
            }
            else
            {
                init();
                Console.WriteLine("登録されたエイリアスはありません。");
            }
        }
        static FileInfo ReadSetting()
        {
            var appDir = new DirectoryInfo(System.AppDomain.CurrentDomain.BaseDirectory);
            return new FileInfo($"{appDir}\\setting.json");
        }
    }
}
