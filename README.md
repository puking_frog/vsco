vsco
====

## Overview

vsco saves the directory or file path as an alias and opens that path using the alias in Visual Studio code.


## Requirement

 - Visula Studio Code
 - Powershell 5
 - .NET Core Runtime 3.0.1


## Instllation

 1. Downloading vsco
 2. Add the path to vsco to your Path environment variable.
 

## Language
 - Currently Japanese only


## Usage

  - Displays the registered path selection list and opens the path selected with VSCode
  ```
  vsco [ select | -s ]
  ```
  
  
  - Open alias path or specified path in VSCode
  ```
  vsco [ open | -o ] <alias>
  
  ```
  
  
  - Register path as an alias
  ```
  vsco [ add | -a ] <alias> <path>
  
  ```
  
  
  - Show command list
  ```
  vsco { help | -h }
  
  ```

  
  - Show registered alias and path list
  ```
  vsco { list | -l }
  
  ```

  - Remove a registered alias

  ```  
  vsco { remove | -r } <alias>

  ```
  
  
  - Change the alias name associated with the path
  ```
  vsco { rename | -rn } <currentAlias> <newAlias>
  
  ```

  - Open the registered alias data file (.json)
  ```
  vsco { vscoSetting | -vs }
  
  ```
