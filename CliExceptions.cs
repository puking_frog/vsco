using System;
using System.Runtime.Serialization;

namespace Clitool
{
    public class InvaliCliOpitonsException : System.Exception
    {
        public InvaliCliOpitonsException(): base()
        {
        }

        public InvaliCliOpitonsException(string message): base(message)
        {
        }

        public InvaliCliOpitonsException(string message, Exception innerException): base(message, innerException)
        {
        }

        protected InvaliCliOpitonsException(SerializationInfo info, StreamingContext context): base(info, context)
        {
        }
    }
    public class InvaliCliCommandException : System.Exception
    {
        public InvaliCliCommandException(): base()
        {
        }

        public InvaliCliCommandException(string message): base(message)
        {
        }

        public InvaliCliCommandException(string message, Exception innerException): base(message, innerException)
        {
        }

        protected InvaliCliCommandException(SerializationInfo info, StreamingContext context): base(info, context)
        {
        }
    }
}