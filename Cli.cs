﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Clitool
{
    public class Cli
    {
        public string name;
        public List<Command> commands = new List<Command>();

        public Cli()
        {
            this.commands.Add(new Command()
            {
                name = "help",
                description = "ヘルプ",
                alias = new List<string>(){
                "-h"
            },
                execute = (List<string> options) =>
                {
                    showHelp();
                }
            });
        }

        public void executeCommand(string[] args)
        {

            string commandName = null;
            string alias = null;
            bool isNoCommand = false;

            if (commands == null)
            {
                throw new InvaliCliCommandException("有効なコマンドがありません。");
            }
            if (args.Length > 0)
                commandName = commands.Select(x => x.name).FirstOrDefault(x => x?.Equals(args[0]) ?? false);

            if (commandName == null)
            {

                var isAlias = false;
                if (args.Length > 0)
                {
                    isAlias = commands.FirstOrDefault(x => x.alias.Contains(args[0])) != null;
                }
                if (isAlias)
                    alias = args[0];
                else
                    isNoCommand = true;
            }

            List<string> options = new List<string>();
            if (args?.Length > 0)
            {
                if (isNoCommand)
                    options = args.ToList();
                else if (args?.Length > 1)
                    options = args.ToList().GetRange(1, args.Length - 1);
            }

            var targetCommands = commands.Where(x => x.alias.Contains(alias) ? true : x.name?.Equals(commandName) ?? isNoCommand);
            Command targetCommand;
            if (targetCommands.Count() > 1)
            {
                targetCommand = targetCommands.SingleOrDefault(x => x.options.Where(y => y.require).Count().Equals(options.Count)) ??
                                targetCommands.SingleOrDefault(x => x.options.Where(y => y.require).Count() <= options.Count && options.Count <= x.options.Count);

                if (targetCommand == null)
                {
                    throw new InvaliCliCommandException("該当するコマンドがありません。 help または -h コマンドでヘルプを参照してください");
                }
            }
            else if (targetCommands.Count() == 1)
            {
                targetCommand = targetCommands.Single();
            }
            else
            {
                throw new InvaliCliCommandException("該当するコマンドがありません。 help または -h コマンドでヘルプを参照してください");
            }


            var targetRequeireCommand = targetCommand.options.Where(x => x.require).ToList();

            // 引数が多すぎる
            if (targetCommand.options.Count < options.Count)
            {
                throw new InvaliCliOpitonsException("指定されているオプションが多すぎます。");
            }
            // 引数が少なすぎる
            if (targetRequeireCommand.Count > options.Count)
            {
                throw new InvaliCliOpitonsException("必須のオプションが指定されていません。");
            }

            targetCommand.execute(options);
        }

        private void showHelp()
        {
            Console.WriteLine("\nCommand List\n\n");
            this.commands.OrderBy(x => x.name).ToList().ForEach(x => x.showHelp(this.name));
        }

    }

    public class Command
    {
        public string name = null;
        public List<string> alias = new List<string>();
        public string description;
        public List<CommandOption> options;
        public delegate void Execute(List<string> options = null);
        public Execute execute;

        public Command()
        {
            this.description = "Command description";
            this.options = new List<CommandOption>();
        }
        public void showHelp(string cli)
        {
            var alias = String.Join(" | ", this.alias);
            string commnadString;
            if (this.name == null)
            {
                commnadString = this.alias.Count > 0 ? $" [ {alias} ] " : " ";
            }
            else
            {
                commnadString = this.alias.Count > 0 ? $" {{ {this.name} | {alias} }} "
                                : $" {this.name} ";
            }

            var command = cli + commnadString + String.Join("", this.options.Select(x => x.require ? $"<{x.name}> " : $"[<{x.name}>]　"));
            Console.WriteLine($"  {command}: {this.description}\n");
            this.options.ForEach(x => x.showHelp());
            Console.WriteLine("\n");
        }
    }

    public class CommandOption
    {
        public string name = "";
        public string description = "";
        public bool require = false;

        public void showHelp()
        {
            Console.WriteLine($"\t{this.name}: {this.description}");
        }
    }
}